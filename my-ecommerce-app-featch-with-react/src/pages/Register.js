import NavRegister from "../components/NavRegister";
import Footer from "../components/Footer";
import { Fragment } from 'react';
import RegisterComp from "../components/RegisterComp"


export default function Register(){
  return (
    <Fragment>
    <NavRegister />
    <RegisterComp />
    <Footer />
    </Fragment>
    )
}