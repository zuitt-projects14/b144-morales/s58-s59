import { Fragment } from 'react';
import Slogan from "../components/Slogan";
import Featured from "../components/Featured";
import NewArrival from "../components/NewArrival";
import AppNavbar from "../components/AppNavbar";
import Footer from "../components/Footer";




export default function Home(){

	


	return (
		
		<Fragment>
			<AppNavbar />
			<Slogan />
			<Featured />
			<NewArrival />
			<Footer />
		</Fragment>
	)
}