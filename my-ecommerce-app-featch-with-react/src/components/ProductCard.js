//import state hook from react
//import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Row, Col, Card } from "react-bootstrap";
import Button  from "react-bootstrap/Button";
import { Link } from "react-router-dom";

export default function ProductCard({productProp}){
//Checks to see if the data was successfully passed
/*console.log(props);
console.log(typeof props);*/

const {_id, name, description, price, imageUrl} = productProp;

//Use state hook in this component to be able to store its state
//states are used to keep track of the information         
 /*Syntax   : 
      const [getter, setter] = useState(initialGetterValue)    ;
    */
    /*const [count, setCount] = useState(0);

    console.log(useState(0));*/

    //Function to keep track of the enrollees for a course
     /*function enroll(){
     	setCount(count + 1);
     	/*console.log("Enrollees: " + count);
     }*/
     /*const [count, setCount] = useState(0);
    console.log(useState(0));
     const [seat, setSeat] = useState(30);
     const [isOpen, setIsOpen] = useState(false)*/


     /*function enroll(){    	
     	setSeat(seat - 1)
     	setCount(count + 1)	
     }*/
// Define a useEffect hook to have thye courseCard component perform a certain task after every DOM update.
/*useEffect(() => {
	if(seat === 0){
		setIsOpen(true);
	}
}, [seat])*/


 return (

  
    <Col xs={12} md={4} className="mt-5">
          <Card className="cardHighlight p-3">
          <img src={imageUrl} alt="Logo" />
  <Card.Body>
    <Card.Title>
      <h3 className="text-center">{name}</h3>
    </Card.Title>
    <Card.Subtitle>Description:</Card.Subtitle>
    <Card.Text>{description}</Card.Text>
    <Card.Subtitle>Price:</Card.Subtitle>
    <Card.Text>PHP {price}</Card.Text>
    


    
  </Card.Body>
</Card>
</Col>


    
		)    
}









