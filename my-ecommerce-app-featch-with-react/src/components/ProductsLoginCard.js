//import state hook from react
//import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Row, Col, Card } from "react-bootstrap";
/*import Button  from "react-bootstrap/Button";*/
import { Link } from "react-router-dom";
import { Form, Button, Container } from 'react-bootstrap';
import { useCart } from "react-use-cart"




export default function ProductsLoginCard({productProp}){

const { addItem } = useCart();




//Checks to see if the data was successfully passed
/*console.log(props);
console.log(typeof props);*/

const {_id, name, description, price, imageUrl} = productProp;



 return (

    <Col xs={12} md={4} className="mt-5">
    
          <Card className="cardHighlight p-3">
          <img className="small" src={imageUrl} alt="Logo" />
          
  <Card.Body>

    <Card.Title>
      <h3 className="text-center" >{name}</h3>
    </Card.Title>
    <Card.Subtitle className="text-center">Description:</Card.Subtitle>
    <Card.Text className="text-center">{description}</Card.Text>
    <Card.Subtitle className="text-center">Price:</Card.Subtitle>
    <Card.Text className="text-center">PHP {price}</Card.Text>
    
  



  </Card.Body>
<Link className="btn btn-success btn-block" to={`/api/product/products/${_id}`}>Details</Link>  
  
</Card>
</Col>


   
		)    
}


