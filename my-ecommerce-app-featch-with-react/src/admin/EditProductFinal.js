import AdminPageAfterLoginViewNav from "./adminNav/AdminPageAfterLoginViewNav";
import AdminFooter from "./AdminFooter";
import { Fragment } from 'react';
import EditProduct from "./EditProduct"


export default function AddProductFinal(){
  return (
    <Fragment>
    <AdminPageAfterLoginViewNav />
    <EditProduct />
    <AdminFooter />
    </Fragment>
    )
}