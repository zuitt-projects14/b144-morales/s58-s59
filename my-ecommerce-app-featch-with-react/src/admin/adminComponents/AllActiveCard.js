//import state hook from react
//import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Row, Col, Card } from "react-bootstrap";
/*import Button  from "react-bootstrap/Button";*/
import { Link } from "react-router-dom";
import { Form, Button, Container } from 'react-bootstrap';
import { useCart } from "react-use-cart"




export default function AllActiveCard({productProp}){

const { addItem } = useCart();




//Checks to see if the data was successfully passed
/*console.log(props);
console.log(typeof props);*/

const {_id, name, description, price, imageUrl, quantity, isActive} = productProp;



 return (

    <Col xs={12} md={4} className="mt-5">
    
          <Card className="cardHighlight p-3">
          <img className="small" src={imageUrl} alt="Logo" />
          
  <Card.Body>

    <Card.Title>
      <h3 className="text-center" >{name}</h3>
    </Card.Title>
    <Card.Subtitle>Description:</Card.Subtitle>
    <Card.Text >{description}</Card.Text>
    <Card.Subtitle>Quantity:</Card.Subtitle>
    <Card.Text >{quantity}</Card.Text>
    <Card.Subtitle >isActive:</Card.Subtitle>
    <Card.Text>{isActive}</Card.Text>
    <Card.Subtitle >Price:</Card.Subtitle>
    <Card.Text>PHP {price}</Card.Text>
    
  



  </Card.Body>
<Link className="btn btn-success btn-block my-3" to={`/archieved/${_id}`}>ARCHIEVED</Link>  
<Link className="btn btn-success btn-block" to={`/admin/edit/${_id}`}>EDIT</Link>  
  
</Card>
</Col>


   
		)    
}