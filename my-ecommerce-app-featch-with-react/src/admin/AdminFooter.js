import React from "react";



export default function AdminFooter() {
return (


	<div className="foot container-fluid">
    <footer className="p-3 text-white">
    <div className="d-flex justify-content-center my-4">
        <a href="https://www.facebook.com/matthew.yabut96" target="_blank" className="mx-3">
          <i className="fab fa-facebook fab-link"></i>
        </a>
        <a href="https://gitlab.com/morales.donald1025" target="_blank" className="mx-3">
          <i className="fab fa-gitlab fab-link"></i>
        </a>
        <a href="https://www.linkedin.com/in/donald-morales-b2043a1b9/" target="_blank" className="mx-3">
          <i className="fab fa-linkedin fab-link"></i>
        </a>
      </div>
    
      
        <div className="row justify-content-center">
          <div className="col-md-2">
        <p className="text-center">
          <i className="fas fa-phone"> +63 930 237 9782</i>
        </p>
      </div>
      <div className="col-md-3">
        <p className="text-center">
          <i className="far fa-envelope"> morales.donald1025@gmail.com</i>
        </p>
      </div>
      </div>
      <hr className="border border-white" />
  
      
      <div className="row justify-content-center">
        <div className="col-md-3">
          <p className="text-center text-white">&copy; 2021 Donald Morales</p>
        </div>
        <div className="col-md-3">
          <p className="text-center text-white">All Rights Reserved</p>		
        </div>
      </div>
    </footer>
  </div>
)
}
